function validateNV(tkNV, tenNV, email, matKhau, ngayLam, luongCB, chucVu, gioLam){
    // Validate tài khoản
    if (tkNV === "" || tkNV.length < 4 || tkNV.length > 6) {
        Swal.fire({
            title: 'Error!',
            text: 'Tài khoản không được trống và phải có từ 4 đến 6 ký tự',
            icon: 'error',
            confirmButtonText: 'Đóng'
        })
        return -1;
    }

    // Validate tên nhân viên
    if (tenNV === "" || !/^[a-zA-Z\s]+$/.test(tenNV)) {
        Swal.fire({
            title: 'Error!',
            text: 'Tên nhân viên không được trống và chỉ chứa ký tự chữ',
            icon: 'error',
            confirmButtonText: 'Đóng'
        })
        return -1;
    }

    // Validate email
    if (email === "" || !/\S+@\S+\.\S+/.test(email)) {
        Swal.fire({
            title: 'Error!',
            text: 'Email không hợp lệ',
            icon: 'error',
            confirmButtonText: 'Đóng'
        })
        return -1;
    }

    // Validate mật khẩu
    if (matKhau === "" || !/^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[!@#$%^&*()]).{6,10}$/.test(matKhau)) {
        Swal.fire({
            title: 'Error!',
            text: 'Mật khẩu không hợp lệ. Yêu cầu từ 6 đến 10 ký tự, bao gồm chữ hoa, chữ thường, số và ký tự đặc biệt',
            icon: 'error',
            confirmButtonText: 'Đóng'
        })
        return -1;
    }

    // Validate ngày làm
    if (ngayLam === "" || !/^\d{2}\/\d{2}\/\d{4}$/.test(ngayLam)) {
        Swal.fire({
            title: 'Error!',
            text: 'Ngày làm không hợp lệ. Yêu cầu định dạng dd/mm/yyyy',
            icon: 'error',
            confirmButtonText: 'Đóng'
        })
        return -1;
    }

    // Validate lương cơ bản
    if (luongCB === "" || isNaN(luongCB) || luongCB < 1000000 || luongCB > 20000000) {
        Swal.fire({
            title: 'Error!',
            text: 'Lương cơ bản không hợp lệ. Yêu cầu từ 1.000.000 đến 20.000.000',
            icon: 'error',
            confirmButtonText: 'Đóng'
        })
        return -1;
    }

    // Validate chức vụ
    if (chucVu === "" || !/^(Sếp|Trưởng phòng|Nhân viên)$/.test(chucVu)) {
        Swal.fire({
            title: 'Error!',
            text: 'Chức vụ không hợp lệ. Yêu cầu là Sếp, Trưởng phòng hoặc Nhân viên',
            icon: 'error',
            confirmButtonText: 'Đóng'
        })
        return -1;
    }

    // Validate số giờ làm
    if (gioLam === "" || isNaN(gioLam) || gioLam < 80 || gioLam > 200) {
        Swal.fire({
            title: 'Error!',
            text: 'Số giờ làm không hợp lệ. Yêu cầu từ 80 đến 200 giờ',
            icon: 'error',
            confirmButtonText: 'Đóng'
        })
        return -1;
    }
}
