function NhanVien(_tk, _ten, _email, _matKhau, _ngayLam, _luongCB, _chucVu, _gioLam) {
    NhanVien.currentId++;
    this.id = NhanVien.currentId;
    this.tkNV = _tk;
    this.tenNV = _ten;
    this.email = _email;
    this.matKhau = _matKhau;
    this.ngayLam = _ngayLam;
    this.luongCB = _luongCB;
    this.chucVu = _chucVu;
    this.gioLam = _gioLam;
    this.tongLuong = 0;
    this.xepLoai = '';

    this.tinhTongLuong = function () {
        if (this.chucVu == 'Sếp') {
            this.tongLuong = this.luongCB * 3;
        } else if (this.chucVu == 'Trưởng phòng') {
            this.tongLuong = this.luongCB * 2;
        } else if (this.chucVu == 'Nhân viên') {
            this.tongLuong = this.luongCB * 1;
        }
        return this.tongLuong;
    };

    this.xepLoaiNV = function () {
        if (this.gioLam >= 192) {
            this.xepLoai = 'Xuất sắc';
        } else if (this.gioLam >= 176) {
            this.xepLoai = 'Giỏi';
        } else if (this.gioLam >= 160) {
            this.xepLoai = 'Khá';
        } else {
            this.xepLoai = 'Trung bình';
        }
        return this.xepLoai;
    };
}

NhanVien.currentId = 0;