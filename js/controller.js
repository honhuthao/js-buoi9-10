function renderDSNV(dsnv){
    var content = "";
    for(var i = 0; i < dsnv.length; i++){
        var nv = dsnv[i];
        var tongLuong = new Intl.NumberFormat('vi-VN', { style: 'currency', currency: 'VND' }).format(nv.tinhTongLuong());
        content += `
            <tr>
                <td>${nv.tkNV}</td>
                <td>${nv.tenNV}</td>
                <td>${nv.email}</td>
                <td>${nv.ngayLam}</td>
                <td>${nv.chucVu}</td>
                <td>${tongLuong}</td>
                <td>${nv.xepLoaiNV()}</td>
                <td>
                    <button class="btn btn-danger" onclick="xoaNhanVien('${nv.id}')">Xóa</button>
                    <button class="btn btn-success" onclick="timNVTheoID('${nv.id}')">Chỉnh sửa</button>
                </td>
            </tr>
        `;
    }
    document.getElementById("tableDanhSach").innerHTML = content;
}

function showThongTinNV(nv){
    // Set the input field values
    document.getElementById("edit_id").value = nv.id;
    document.getElementById("edit_tknv").value = nv.tkNV;
    document.getElementById("edit_name").value = nv.tenNV;
    document.getElementById("edit_email").value = nv.email;
    document.getElementById("edit_password").value = nv.matKhau;
    document.getElementById("edit_datepicker").value = nv.ngayLam;
    document.getElementById("edit_luongCB").value = nv.luongCB;
    document.getElementById("edit_chucvu").value = nv.chucVu;
    document.getElementById("edit_gioLam").value = nv.gioLam;
    // Show the modal
    $("#editModal").modal("show");
}

function layThongTinNV(){
    var tk = document.getElementById("tknv").value;
    var hoTen = document.getElementById("name").value;
    var email = document.getElementById("email").value;
    var matKhau = document.getElementById("password").value;
    var ngayLam = document.getElementById("datepicker").value;
    var luongCB = document.getElementById("luongCB").value *1;
    var chucVu = document.getElementById("chucvu").value;
    var gioLam = document.getElementById("gioLam").value *1;
    return new NhanVien(tk, hoTen, email, matKhau, ngayLam, luongCB, chucVu, gioLam);
}

