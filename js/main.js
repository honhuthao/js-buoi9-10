var dsnv = [];
var dataJson = localStorage.getItem("DSNV");
if (dataJson != null) {
    dsnv = JSON.parse(dataJson).map(function (item) {
        return new NhanVien(item.tkNV, item.tenNV, item.email, item.matKhau, item.ngayLam, item.luongCB, item.chucVu, item.gioLam, item.tongLuong, item.xepLoai);
    });
    renderDSNV(dsnv);
}

function themNV(){
    var nv = layThongTinNV();
    var validate = validateNV(nv.tkNV, nv.tenNV, nv.email, nv.matKhau, nv.ngayLam, nv.luongCB, nv.chucVu, nv.gioLam);
    if (validate == -1) {
        return;
    }
    dsnv.push(nv);
    $("#myModal").modal("hide");

    renderDSNV(dsnv);
    var dataJson = JSON.stringify(dsnv);
    localStorage.setItem("DSNV", dataJson);
}

function xoaNhanVien(tk){
    var index = dsnv.findIndex(function(item){
        return item.tk == tk;
    });
    dsnv.splice(index, 1);
    renderDSNV(dsnv);
    var dataJson = JSON.stringify(dsnv);
    localStorage.setItem("DSNV", dataJson);
}

function timNVTheoID(id){
    var index = dsnv.findIndex(function(item){
        return item.id == id;
    });
    var nv = dsnv[index];
    showThongTinNV(nv);
}

function capNhatNV(){
    var tkNV = document.getElementById("edit_tknv").value;
    var tenNV = document.getElementById("edit_name").value;
    var email = document.getElementById("edit_email").value;
    var matKhau = document.getElementById("edit_password").value;
    var ngayLam = document.getElementById("edit_datepicker").value;
    var luongCB = document.getElementById("edit_luongCB").value *1;
    var chucVu = document.getElementById("edit_chucvu").value;
    var gioLam = document.getElementById("edit_gioLam").value *1;
    
    var validate = validateNV(tkNV, tenNV, email, matKhau, ngayLam, luongCB, chucVu, gioLam);
    if (validate == -1) {
        return;
    }
    var id = document.getElementById("edit_id").value;
    var index = dsnv.findIndex(function(item) {
        return item.id == id;
    });
    if (index != -1) {
        var nv = dsnv[index];
        nv.tkNV = tkNV;
        nv.tenNV = tenNV;
        nv.email = email;
        nv.matKhau = matKhau;
        nv.ngayLam = ngayLam;
        nv.luongCB = luongCB;
        nv.chucVu = chucVu;
        nv.gioLam = gioLam;

        nv.tinhTongLuong();
        nv.xepLoaiNV();

        $("#editModal").modal("hide");
        renderDSNV(dsnv);
        var dataJson = JSON.stringify(dsnv);
        localStorage.setItem("DSNV", dataJson);

    } else {
        console.log("Không tìm thấy nhân viên có id: " + id);
    }
}

var searchInput = document.getElementById('searchXepLoai');
searchInput.addEventListener('input', function() {
  timNVTheoXepLoai();
});

function timNVTheoXepLoai(){
    var xepLoai = document.getElementById("searchXepLoai").value;
    if(xepLoai == "") {
        renderDSNV(dsnv);
        return;
    }else{
        var dsnvTim = dsnv.filter(function(item){
            var nvXepLoai = item.xepLoai.toLowerCase();
            for(var i = 0; i < nvXepLoai.length; i++){
                if(nvXepLoai.indexOf(xepLoai.toLowerCase()) != -1){
                    return true;
                }
            }
        });
        renderDSNV(dsnvTim);
    }
}